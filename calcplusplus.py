'modulos'
import sys
import calcoohija
import csv

'fichero'
fichero = str(sys.argv[1])
with open(fichero) as f:
    for operacion in f:
        operacion_split = operacion.split(",")

        'operando'
        operando = operacion_split[0]

        'operadores'
        operadores = operacion_split[1:]

        'objects'
        calcuplus = calcoohija.CalculadoraHija()

        result = operadores[0]

        for numero in operadores[1:]:
            'operations'
            if operando == "suma":
                result = calcuplus.sum(float(result), float(numero))
            elif operando == "resta":
                result = calcuplus.rest(float(result), float(numero))
            elif operando == "divide":
                result = calcuplus.div(float(result), float(numero))
            elif operando == "multiplica":
                result = calcuplus.mul(float(result), float(numero))
            else:
                sys.exit('Operación sólo puede ser sumar, rest, multi o div.')

        print(result)

f.close()
