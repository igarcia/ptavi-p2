

import sys

""" CREATE THE CLASS NAME 'Calculadora' """


class Calculadora():

    'suma'
    def sum(self, ope_1, ope_2):
        return ope_1 + ope_2

    'resta'
    def rest(self, ope_1, ope_2):
        return ope_1 - ope_2


""" MAIN PROGRAM """
if __name__ == "__main__":
    try:
        ope_1 = int(sys.argv[1])
        ope_2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    'objects'
    objeto = Calculadora()

    'operations'
    if sys.argv[2] == "suma":
        result = objeto.sum(ope_1, ope_2)
    elif sys.argv[2] == "resta":
        result = objeto.rest(ope_1, ope_2)
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(result)
