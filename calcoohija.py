import sys
import calcoo

""" CREATE THE CLASS NAME 'CalculadoraHija' """


class CalculadoraHija(calcoo.Calculadora):
    'division'
    def div(self, ope_1, ope_2):
        try:
            return ope_1 / ope_2
        except ZeroDivisionError:
            sys.exit("Division by zero is not allowed.")

    'multiplicacion'
    def mul(self, ope_1, ope_2):
        return ope_1 * ope_2


""" MAIN PROGRAM """
if __name__ == "__main__":
    try:
        ope_1 = int(sys.argv[1])
        ope_2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    'objects'
    calculadora = CalculadoraHija()

    'operations'
    if sys.argv[2] == "suma":
        result = calculadora.sum(ope_1, ope_2)
    elif sys.argv[2] == "resta":
        result = calculadora.rest(ope_1, ope_2)
    elif sys.argv[2] == "divide":
        result = calculadora.div(ope_1, ope_2)
    elif sys.argv[2] == "multiplica":
        result = calculadora.mul(ope_1, ope_2)
    else:
        sys.exit('Operación sólo puede ser sumar,restar,mul o div.')

    print(result)
